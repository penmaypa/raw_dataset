#=================================
#   ---  OUR FUNCTIONS  ----
#
# This R-Code contains a list of customized functions
#   that can be re-used for a particular purposes.
#
#=================================


cutdown_data <- function(df, start_row, end_row){
  df_output <- df[start_row:end_row,]
  return(df_output)
}


